/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['./trimtex_pos_cashsales_lib.js'],

function(cashSale, runtime, search) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	var ctx = scriptContext;
    	
    	if(ctx.type == 'view' && !ctx.newRecord.getValue({fieldId: 'custrecord_sta_tx_pos_cs_cashsale'})){
    		var form = ctx.form;
    		form.clientScriptModulePath = './trimtex_pos_cashsales_lib.js';
    		
    		var createCashSaleButton = form.addButton({
    			id: 'custpage_createcashsale',
    			label: 'Create Cash Sale',
    			functionName: 'request('+ ctx.newRecord.id +')'
    		})
    	}
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	var ctx = scriptContext, logObj = {};
    	
    	if(ctx.type == 'edit' && !ctx.newRecord.getValue({fieldId: 'custrecord_sta_tx_pos_cs_cashsale'})){
    		try{
    			var cashSaleId = cashSale.create(ctx.newRecord.id);
    			
    			logObj.type = "SUCCESS";
    			logObj.title = "Record Created";
    			logObj.description = "Cash sale has been crated:" + cashSaleId;
    			
    			cashSale.log(ctx.newRecord.id, logObj);
    		}catch(e){
    			logObj.type = "ERROR";
    			logObj.title = e.name;
    			logObj.description = e;
    				
    			cashSale.log(ctx.newRecord.id, logObj);
    		}
    		
    	}
    }

    return {
        beforeLoad: beforeLoad,
        //beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
