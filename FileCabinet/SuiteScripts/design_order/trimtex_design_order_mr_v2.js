/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */
define(['N/record', 'N/search', 'N/runtime', './trimtex_design_order_cs_v2.js', 'N/task'],
/**
 * @param {record} record
 * @param {search} search
 */
function(record, search, runtime, designorder, task) {
	var RECORD_TYPE = {'Assembly' : 'assemblyitem'}
	var JUNIOR_SIZE = [];//['1', '2', '3', '4', '5'];
	var INTERMEDIATE_MASTER_SIZE = [];//['18', '19', '20', '21'];
	var INTERMEDIATE_MASTER_SIZE_DEFAULT = '18';
	var INTERMEDIATE_MASTER_JUNIOR_SIZE_DEFAULT = '19';
    /**
     * Marks the beginning of the Map/Reduce process and generates input data.
     *
     * @typedef {Object} ObjectRef
     * @property {number} id - Internal ID of the record instance
     * @property {string} type - Record type id
     *
     * @return {Array|Object|Search|RecordRef} inputSummary
     * @since 2015.1
     */
    function getInputData() {
    	
    	var doId = runtime.getCurrentScript().getParameter({name : 'custscript_sta_tx_do_id_mr'});
    	
    	record.submitFields({
    		type : 'customrecord_sta_tx_do',
    		id : doId,
    		values : {
    			custrecord_sta_dx_do_matrix_status : 'CREATING MATRIX SUBITEM'
    		}
    	});
    	
    	return createMatrixSubitem(doId);
    }

    /**
     * Executes when the map entry point is triggered and applies to each key/value pair.
     *
     * @param {MapSummary} context - Data collection containing the key/value pairs to process through the map stage
     * @since 2015.1
     */
    function map(context) {
    	var valueObj = JSON.parse(context.value);
    	
    	log.debug('map ' + valueObj.fields.parent, valueObj);
    	
    	context.write(valueObj.fields.parent, valueObj);
    }

    /**
     * Executes when the reduce entry point is triggered and applies to each group.
     *
     * @param {ReduceSummary} context - Data collection containing the groups to process through the reduce stage
     * @since 2015.1
     */
    function reduce(context) {
    	for(var i = 0; i < context.values.length; i++){
    		
    		var matrixObj = JSON.parse(context.values[i]);
    		
    		var matrixSubRec = record.create({type : matrixObj.type});
    		
    		Object.keys(matrixObj.fields).forEach(function(field){
    			matrixSubRec.setValue(field, matrixObj.fields[field]);
    		});
    		
    		//Set value for online price
    		/*matrixSubRec.selectLine('price' + matrixObj.price.currency, 
    				matrixSubRec.findSublistLineWithValue('price' + matrixObj.price.currency, 'pricelevel', 5));
    		matrixSubRec.setCurrentSublistValue('price' + matrixObj.price.currency, 'price_1_', matrixObj.price.rate);
    		matrixSubRec.setCurrentSublistValue('price' + matrixObj.price.currency, 'quantity', 0);
    		matrixSubRec.commitLine('price' + matrixObj.price.currency);*/
    		
    		matrixSubRec.setSublistValue('price' + matrixObj.price.currency, 'price_1_', 
    				matrixSubRec.findSublistLineWithValue('price' + matrixObj.price.currency, 'pricelevel', 1), matrixObj.price.listPrice)
    		
    		matrixSubRec.setSublistValue('price' + matrixObj.price.currency, 'price_1_', 
    				matrixSubRec.findSublistLineWithValue('price' + matrixObj.price.currency, 'pricelevel', 5), matrixObj.price.rate)
    		
    		
			var matrixSubRecId = matrixSubRec.save({enableSourcing : true});
    		
    		record.submitFields({
        		type : 'customrecord_sta_tx_itemdesign',
        		id : matrixObj.design.id,
        		values : {
        			custrecord_sta_tx_itemdesign_matrix : true
        		}
        	});
    		
			log.debug('reduce ' + matrixSubRecId, matrixObj);
			context.write(matrixSubRecId, matrixObj);
    		
    	}
    }


    /**
     * Executes when the summarize entry point is triggered and applies to the result set.
     *
     * @param {Summary} summary - Holds statistics regarding the execution of a map/reduce script
     * @since 2015.1
     */
    function summarize(summary) {
    	
    	var doId = runtime.getCurrentScript().getParameter({name : 'custscript_sta_tx_do_id_mr'});
    	summary.output.iterator().each(function(key, value){
    		
    		var matrixObj = JSON.parse(value);
    		var designMatrixItemLink = record.create({type : 'customrecord_sta_tx_dolink'});
				
			designMatrixItemLink.setValue('custrecord_sta_tx_dolink_item', key);
			designMatrixItemLink.setValue('custrecord_sta_tx_dolink_do', doId);
			designMatrixItemLink.setValue('custrecord_sta_tx_dolink_do_design', matrixObj.design.id);
			designMatrixItemLink.setValue('custrecord_sta_tx_dolink_do_item', matrixObj.design.item);
			
			designMatrixItemLink.save();
			
			/*var intermediateItem = '';
			if(matrixObj.design.priceParam == 1){
				summary.output.iterator().each(function(key, value){
					
					var intermediateItemObj = JSON.parse(value);
					
					if(matrixObj.fields.custitem_sta_tx_size == INTERMEDIATE_MASTER_SIZE_DEFAULT && matrixObj.fields.custitem_sta_tx_intermediate_checkbox && matrixObj.design.id == intermediateItemObj.design.id){
						intermediateItem = key;
					}
					return true;
				});
			}else{
				summary.output.iterator().each(function(key, value){
					
					var intermediateItemObj = JSON.parse(value);
					
					if(matrixObj.fields.custitem_sta_tx_size == INTERMEDIATE_MASTER_JUNIOR_SIZE_DEFAULT && matrixObj.fields.custitem_sta_tx_intermediate_checkbox && matrixObj.design.id == intermediateItemObj.design.id){
						intermediateItem = key;
					}
					return true;
				});
			}
			
			record.submitFields({
	    		type : matrixObj.type,
	    		id : key,
	    		values : {
	    			custitem_sta_tx_link_intermediate_item : intermediateItem
	    		}
	    	});*/
			
			return true;
    	});
    	
    	var updateMatrixTask = task.create({
			taskType : task.TaskType.MAP_REDUCE,
			scriptId : 'customscript_sta_tx_do_create_matrix_mr2',
			params : {
				custscript_sta_tx_do_id_mr_2 : doId
			}
		});
		
		
		var updateMatrixTaskStatus = task.checkStatus(updateMatrixTask.submit());
    	
    	summary.reduceSummary.errors.iterator().each(function(key, error){
    		log.error('ERROR_CREATING_SUBITEM', key + ' : ' + error);
    	});
    	
    	record.submitFields({
    		type : 'customrecord_sta_tx_do',
    		id : doId,
    		values : {
    			custrecord_sta_dx_do_matrix_status : 'CREATING INTERMEDIATE MASTER'
    		}
    	});
    }
    
    var createMatrixSubitem = function(designOrderId){
    	var matrixItemsObj = [];
    	var designNumberObj = designorder.getDesignNumbers(designOrderId);    	
    	log.debug(designNumberObj);
    	var customlist_sta_tx_sizelistSearchObj = search.create({
			   type: "customlist_sta_tx_sizelist", filters: [],
			   columns: [search.createColumn({name: "name"}),search.createColumn({name: "abbreviation"})]
			});
		
    	//var searchResultCount = customlist_sta_tx_sizelistSearchObj.runPaged().count;
    	//log.debug("customlist_sta_tx_sizelistSearchObj result count",searchResultCount);

    	
		var customlist_sta_tx_specialsizeSearchObj = search.create({
			   type: "customlist_sta_tx_speciallength", filters: [],
			   columns: [search.createColumn({name: "name"}),search.createColumn({name: "abbreviation"})]
			});
		
		//var searchResultCount = customlist_sta_tx_specialsizeSearchObj.runPaged().count;
    	//log.debug("customlist_sta_tx_specialsizeSearchObj result count",searchResultCount);
		
    	//***size scale - new
    	var customrecord_sta_size_scaleSearchObj = search.create({
			   type: "customrecord_sta_tx_size_scale", filters: [],
			   columns: [search.createColumn({name: "name"}),search.createColumn({name: "custrecord_sta_tx_size_scale_sizes"})]
			});
    	
		var customrecord_sta_tx_dodetSearchObj = search.load('customsearch_sta_tx_do_item_search_2');
		customrecord_sta_tx_dodetSearchObj.filterExpression = [["custrecord_sta_tx_itemdesign_parentlink.custrecord_sta_tx_itemdesign_appr","anyof","2"], "AND", 
		                                      			      ["custrecord_sta_tx_dodet_parentlink", "anyof", designOrderId],"AND",
		                                     			      ["custrecord_sta_tx_itemdesign_parentlink.custrecord_sta_tx_itemdesign_matrix","is","F"]]
		
		//var searchResultCount = customrecord_sta_tx_dodetSearchObj.runPaged().count;
    	//log.debug("customrecord_sta_tx_dodetSearchObj result count",searchResultCount);
		//custrecord_sta_tx_dodet_parentlink
		customrecord_sta_tx_dodetSearchObj.run().each(function(result){
			if(result.getValue({name : 'custrecord_sta_tx_dodet_intermediate'})){
				
				var designNumber = designNumberObj[result.getValue({name : 'name', join: 'CUSTRECORD_STA_TX_ITEMDESIGN_PARENTLINK'})] 
				designNumber = (designNumber) ? designNumber : designorder.createDesignNumber(result.getValue({name : 'name', join: 'CUSTRECORD_STA_TX_ITEMDESIGN_PARENTLINK'}),
						result.getValue({name : 'custrecord_sta_tx_itemdesign_name', join: 'CUSTRECORD_STA_TX_ITEMDESIGN_PARENTLINK'}));
				
				log.debug('designNumber', designNumber);
				log.debug('size scale', result.getValue({name: "custitem_sta_tx_size_scale", join: "CUSTRECORD_STA_TX_DODET_PARENTITEM"}));
				customrecord_sta_size_scaleSearchObj.filterExpression = [["internalid", "anyof", result.getValue({name: "custitem_sta_tx_size_scale", join: "CUSTRECORD_STA_TX_DODET_PARENTITEM"})]]
				
				var scaleSizes = [];
				var searchResultCount = customrecord_sta_size_scaleSearchObj.runPaged().count;
		    	log.debug("customrecord_sta_size_scaleSearchObj result count",searchResultCount);
				
				customrecord_sta_size_scaleSearchObj.run().each(function(sizeScale){
					if(sizeScale.getValue('custrecord_sta_tx_size_scale_sizes')) 
						scaleSizes =  sizeScale.getValue('custrecord_sta_tx_size_scale_sizes').split(',')
					//return true;
				});
				
				log.debug('scaleSizes', scaleSizes);
				
				var parentRec = record.load({type : 'assemblyitem', id : result.getValue({name : 'custrecord_sta_tx_dodet_intermediate'})});
				
				customlist_sta_tx_specialsizeSearchObj.run().each(function(specialSize){
					
					customlist_sta_tx_sizelistSearchObj.run().each(function(size){
						if(scaleSizes.indexOf(size.id) > -1){
							var itemObj = {};
							itemObj.fields = {};
							
							if (((result.getValue({name: "custrecord_sta_tx_dodet_pricepar"}) == 1 || (!result.getValue({name: "custrecord_sta_tx_dodet_pricepar"}))) && JUNIOR_SIZE.indexOf(size.id) == -1 && specialSize.id == 2)){
								itemObj = renderItemRecord(matrixItemsObj, itemObj, result, size, specialSize, designNumber, parentRec);
								matrixItemsObj.push(itemObj);
							}
							
							if (result.getValue({name: "custrecord_sta_tx_dodet_pricepar"}) == 2 && JUNIOR_SIZE.indexOf(size.id) != -1 && specialSize.id == 2){
								itemObj = renderItemRecord(matrixItemsObj, itemObj, result, size, specialSize, designNumber, parentRec);
								matrixItemsObj.push(itemObj);
							}
							
							if (result.getValue({name: "custrecord_sta_tx_dodet_pricepar"}) == 3 && JUNIOR_SIZE.indexOf(size.id) == -1 && specialSize.id != 2){
								itemObj = renderItemRecord(matrixItemsObj, itemObj, result, size, specialSize, designNumber, parentRec);
								matrixItemsObj.push(itemObj);
							}
						}
						return true;
		    		});
					
					return true;
				});

			}
			return true;
		});
		
		return matrixItemsObj;
    };
    
    var renderItemRecord = function(matrixItemsObj, itemObj, result, size, specialSize, designNumber, parentRec){
    	//if(!result.getValue({name: 'custrecord_sta_tx_dodet_intermediate'})) return false;
    			
    	itemObj.type = RECORD_TYPE[result.getValue({name: "type", join: "CUSTRECORD_STA_TX_DODET_PARENTITEM"})];
    	
    	//subsidiary
		itemObj.fields.subsidiary = result.getValue({name: "custrecord_sta_tx_do_sub", join: "custrecord_sta_tx_dodet_parentlink"})
		
		//itemid based on item name template
		itemObj.fields.itemid = parentRec.getValue('matrixitemnametemplate').toString();
		itemObj.fields.itemid  = itemObj.fields.itemid.replace('{custitem_sta_tx_design_number}', result.getValue({name : 'name', join: 'CUSTRECORD_STA_TX_ITEMDESIGN_PARENTLINK'}))
		itemObj.fields.itemid  = itemObj.fields.itemid.replace('{custitem_sta_tx_size}', size.getValue({name : 'abbreviation'}));
		itemObj.fields.itemid  = itemObj.fields.itemid.replace('{custitem_sta_tx_speciallength}', specialSize.getValue({name : 'abbreviation'}));
		itemObj.fields.itemid  = itemObj.fields.itemid.replace('{itemid}', parentRec.getValue('itemid'));
				
    	for(var column in result.columns){
    		if(result.columns[column].join && (result.columns[column].join).toUpperCase() == 'CUSTRECORD_STA_TX_DODET_PARENTITEM'.toUpperCase()){
    			itemObj.fields[result.columns[column].name] = result.getValue(result.columns[column]);
    		}
    	}
    	
    	//teamshop
		itemObj.fields.custitem_sta_teamshop = result.getValue({name : "custrecord_sta_tx_dodet_teamshop"});
		itemObj.fields.custitem_sta_tx_contract_id = result.getValue({name: "custrecord_sta_teamshop_contractid", join: "CUSTRECORD_STA_TX_DODET_TEAMSHOP"});
		itemObj.fields.custitem_sta_designorder = result.getValue({name : "custrecord_sta_tx_dodet_parentlink"});
		
    	delete itemObj.fields.type;
    	delete itemObj.fields.custitem_sta_tx_size;
    	delete itemObj.fields.custitem_sta_tx_speciallength;
    	delete itemObj.fields.expenseaccount;
    	
    	//replace expenseaccount with cogsaccount
		itemObj.fields.cogsaccount = result.getValue({name: 'expenseaccount', join: 'CUSTRECORD_STA_TX_DODET_PARENTITEM'});
		
		//parent
		itemObj.fields.parent = result.getValue({name: 'custrecord_sta_tx_dodet_intermediate'});
		
		//matrix options
		itemObj.fields.matrixtype = 'CHILD'
		itemObj.fields.matrixoptioncustitem_sta_tx_size = size.id;
		itemObj.fields.matrixoptioncustitem_sta_tx_speciallength = specialSize.id;
		itemObj.fields.matrixoptioncustitem_sta_tx_design_number = designNumber;
		
	    //demandware categories
	    itemObj.fields.custitem_sta_tx_dwcat = result.getValue({name: "custrecord_sta_tx_dodet_dwcat"});
	    itemObj.fields.custitem_sta_tx_dwcolor = result.getValue({name: "custrecord_sta_tx_itemdesign_color", join: "CUSTRECORD_STA_TX_ITEMDESIGN_PARENTLINK"});
	    
	    //intermediate
	    itemObj.fields.custitem_sta_tx_intermediate_checkbox = (INTERMEDIATE_MASTER_SIZE.indexOf(size.id) != -1);
	    
	    itemObj.price = {};
		itemObj.price.currency = result.getValue({name: "custrecord_sta_tx_do_curr", join: "custrecord_sta_tx_dodet_parentlink"});
		itemObj.price.rate = result.getValue({name: "custrecord_sta_tx_dodet_rate"});
		itemObj.price.listPrice = result.getValue({name: "custrecord_sta_tx_dodet_list_rate"});
		
		itemObj.design = {};
		itemObj.design.id = result.getValue({name: "internalid", join: "CUSTRECORD_STA_TX_ITEMDESIGN_PARENTLINK"});
		itemObj.design.item = result.id;
		itemObj.design.priceParam = result.getValue({name: "custrecord_sta_tx_dodet_pricepar"});
		
		return itemObj;
    };
    
    var getSizesFromScale = function(sizeScale, sizeScaleSearchObj){
    	
    };
    
    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
    
});
