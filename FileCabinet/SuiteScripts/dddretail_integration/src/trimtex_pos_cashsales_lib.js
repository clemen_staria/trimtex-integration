define(['N/record', 'N/runtime', 'N/https', 'N/url'],

function(record, runtime, https, url) {
	
	function create_cashSale(id){
		if(id){
			var tempCashSaleRec = record.load({type: 'customrecord_sta_tx_pos_cashsales', id: id});
			var cashSaleRec = record.create({type: record.Type.CASH_SALE, isDynamic: true});
			
			if(tempCashSaleRec.getValue({fieldId: 'custrecord_sta_tx_pos_cs_cashsale'})){
				cashSaleRec.setValue({fieldId: 'entity', value: tempCashSaleRec.getValue({fieldId: 'custrecord_sta_tx_pos_cs_customer'})});
				cashSaleRec.setValue({fieldId: 'trandate', value: tempCashSaleRec.getValue({fieldId: 'custrecord_sta_tx_pos_cs_trandate'})});
				cashSaleRec.setValue({fieldId: 'location', value: tempCashSaleRec.getValue({fieldId: 'custrecord_sta_tx_pos_cs_location'})});
				cashSaleRec.setValue({fieldId: 'paymentmethod', value: 1});
				
				for(var i = 0; i < tempCashSaleRec.getLineCount({sublistId: 'recmachcustrecord_sta_tx_pos_parent_cs'}); i++){
					cashSaleRec.selectNewLine({sublistId: 'item'});
					
					cashSaleRec.setCurrentSublistValue('item','item', tempCashSaleRec.getSublistValue({sublistId: 'recmachcustrecord_sta_tx_pos_parent_cs', fieldId: 'custrecord_sta_tx_pos_cs_item', line: i}));
					
					cashSaleRec.setCurrentSublistValue({sublistId: 'item', fieldId: 'quantity', value: 
						tempCashSaleRec.getSublistValue({sublistId: 'recmachcustrecord_sta_tx_pos_parent_cs', fieldId: 'custrecord_sta_tx_pos_item_qty', line: i})});
					
					cashSaleRec.setCurrentSublistValue({sublistId: 'item', fieldId: 'price', value: '-1'});
					
					cashSaleRec.setCurrentSublistValue({sublistId: 'item', fieldId: 'rate', value: 
						tempCashSaleRec.getSublistValue({sublistId: 'recmachcustrecord_sta_tx_pos_parent_cs', fieldId: 'custrecord_sta_tx_pos_cs_item_amt', line: i})});
				
					cashSaleRec.commitLine({sublistId: 'item'});
				}
				
				var cashSaleRecId = cashSaleRec.save();
				
				tempCashSaleRec.setValue({fieldId: 'custrecord_sta_tx_pos_cs_cashsale', value: cashSaleRecId});
				var tempCashSaleRecId = tempCashSaleRec.save();
				log.debug('cashSaleRecId', cashSaleRecId);
				return cashSaleRecId;
			}
		}
	}

	function create_cashSale_SL(id){
		if(id){
			var slURL = url.resolveScript({scriptId: 'customscript_sta_tx_pos_create_cs_sl', deploymentId: 'customdeploy_sta_tx_pos_create_cs_sl', returnExternalUrlNew: true});
			var msgBox = Ext.MessageBox.wait('Please wait while creating cash sale...', 'Create Cash Sale');
			console.log(slURL);
			https.get.promise({
				url: slURL + "&txCsId=" + id
			}).then(function(response){
				msgBox.hide();
				//Ext.MessageBox.alert('Create Cash Sale', response.body);
			}).catch(function onRejected(reason){
				msgBox.hide();
				Ext.MessageBox.alert('Create Cash Sale', reason);
			});
		}
	}
	
	function log_scriptExecution(id, logObj){
		if(id){
			var tempCashSaleRec = record.create({type: 'customrecord_sta_tx_pos_cashsale_logs'});
			tempCashSaleRec.setValue({fieldId: 'custrecord_sta_tx_pos_parent_cs_log', value: id});
			tempCashSaleRec.setValue({fieldId: 'custrecord_sta_tx_pos_log_type', value: logObj.type});
			tempCashSaleRec.setValue({fieldId: 'custrecord_sta_tx_pos_log_title', value: logObj.title});
			tempCashSaleRec.setValue({fieldId: 'custrecord_sta_tx_pos_log_description', value: logObj.description});
			tempCashSaleRec.save();
		}
	}
	
    return {
    	create : create_cashSale,
    	request: create_cashSale_SL,
    	log: log_scriptExecution
    };
    
});
