/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define(['N/url', 'N/record', 'N/https', 'N/ui/message', 'N/search', 'N/runtime'],

function(url, record, https, message, search, runtime) {
	var DESIGN_ORDER_REC_TYPE_ID = 'customrecord_sta_tx_do';
	var DESIGN_ORDER_ITEM_REC_TYPE_ID = 'customrecord_sta_tx_dodet';
	var DESIGN_ORDER_ITEM_DESIGN_REC_TYPE_ID = 'customrecord_sta_tx_itemdesign';
	var DESIGN_ORDER_SUITELET_SCRIPT_ID = 'customscript_tx_design_order_sl';
	var DESIGN_ORDER_ITEM_PERMISSION_ID = 'LIST_CUSTRECORDENTRY599';// LIST_CUSTRECORDENTRY396 in SB
	
	var IO_EXPORT_TEAMSHOP_WEBHOOK_URL = "https://api.integrator.io/v1/exports/5ef2efd415c57e30aa64917f/e83907f20a1c4d5db22e43de4ff138f1/data"
	var IO_EXPORT_DESIGN_ORDER_WEBHOOK_URL = "https://api.integrator.io/v1/exports/5f15bc4a26b1152832ea7f11/4247c392d5774aaf9b4d8b4b9fd56960/data";
	
	var resolveURL = function(scriptId, deploymentId, params){
		return  url.resolveScript({ scriptId : scriptId, deploymentId : deploymentId, params : params });
	}
	//START - User Event button/client functions
    //1 - create design order
    function onCreateDesignOrder(id){
    	Ext.Msg.wait('Design order creation in progress..', 'Create Design Order');

    	var slUrl = resolveURL(DESIGN_ORDER_SUITELET_SCRIPT_ID, 'customdeploy_tx_do_create_design_order', {offerId : id});
    	//console.log(slUrl);
    	var createDesignOrderPromise = https.get.promise({ url : slUrl })
    		.then(function(response){
	    		//Ext.Msg.hide();
	    		//Ext.Msg.wait('Applying default item design...', 'Create Design Order');
	    		/*https.get.promise({
		    		url : resolveURL(DESIGN_ORDER_SUITELET_SCRIPT_ID, 'customdeploy_tx_do_create_item_design', {designOrderId : response.body})
		    	}).then(function(response){
		    		Ext.Msg.hide();
		    		window.open(url.resolveRecord({ recordType : DESIGN_ORDER_REC_TYPE_ID, isEditMode : true,recordId : response.body }), '_blank');
	        	})['catch'](function(reason){
	        		Ext.Msg.hide();
	        		message.create({ title : 'Unable to apply default item design.', message : reason, type : message.Type.WARNING }).show({duration: 5000});
	        	});*/
    			
    			location.reload();
    		
	    	})['catch'](function(reason){
	    		Ext.Msg.hide();
	    		message.create({ title : 'Unable to create design order', message : reason, type : message.Type.WARNING }).show({duration: 5000});
	    	});	
    }
    
    //2 - create matrix subitem
    function onCreateMatrixItem(id, teamshopId){
    	//Ext.Msg.wait('Please wait while creating matrix subitem.', 'Create Matrix Item');
    	var slUrl = resolveURL(DESIGN_ORDER_SUITELET_SCRIPT_ID, 'customdeploy_tx_do_create_matrix_subitem', {designOrderId : id, teamshopId : teamshopId});
    	
    	var createMatrixPromise = https.get.promise({ url : slUrl })	
    		.then(function(response){ location.reload();})
    		['catch'](function(reason){
	    		//Ext.Msg.hide();
	    		var reasonObj = JSON.parse(reason.message);
	    		message.create({ title : reasonObj.name, message : reasonObj.message, type : message.Type.WARNING }).show({duration: 10000});
	    	});    	
    }
    
    //2.1 - get design numbers
    function onGetDesignNumbers(designOrderId){
		var designOrderRec = record.load({ type : DESIGN_ORDER_REC_TYPE_ID, id : designOrderId });
		
		var filterExpression = [], designNumberObj = {};
		
		for(var i = 0; i < designOrderRec.getLineCount('recmachcustrecord_sta_tx_itemdesign_do'); i++){
			if(filterExpression.length > 0) filterExpression.push("OR");
			filterExpression.push(["abbreviation","is", designOrderRec.getSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord_sta_tx_itemdesign_id', i)]);
		}
		
    	var customlist_sta_tx_design_numberSearchObj = search.create({ type: "customlist_sta_tx_design_number", filters: [], columns: [ search.createColumn({ name: "name"})] });
    	customlist_sta_tx_design_numberSearchObj.filterExpression = filterExpression;
    	
    	customlist_sta_tx_design_numberSearchObj.run().each(function(result){
    		// .run().each has a limit of 4,000 results
    		designNumberObj[result.getValue('name').trim()] = result.id;
    		return true;
    	});
    	
    	return designNumberObj;
    }
    
    //2.2 - create/get design number
    function createDesignNumber(abbreviation, name){
    	var designNumberId;
    	try{
    		if(abbreviation){
        		var designNumberRec = record.create({type: 'customlist_sta_tx_design_number'});
        		designNumberRec.setValue('name', abbreviation + ' ' + name);
        		designNumberRec.setValue('abbreviation', abbreviation);
        		designNumberId = designNumberRec.save();
        	}
    	}catch(e){
    		log.debug('e', e);
    	}
    	
    	return designNumberId;
    }
    
    //3 - create default item design
    function onCreateDefaultItemDesign(designOrderId){
    	if(designOrderId){
    		var customrecord_sta_tx_dodetSearchObj = search.create({
    			   type: "customrecord_sta_tx_dodet",
    			   filters:
    			   [
    			      ["custrecord_sta_tx_dodet_parentitem","anyof","@ALL@"], 
    			      "AND", 
    			      ["custrecord_sta_tx_dodet_itemstatus","anyof","@ALL@"], 
    			      "AND", 
    			      ["formulatext: {custrecord_sta_tx_itemdesign_parentlink.name}","isempty",""],
    			      "AND", 
    			      ["custrecord_sta_tx_dodet_parentlink","anyof",designOrderId]
    			   ],
    			   columns: []
    			});
    			customrecord_sta_tx_dodetSearchObj.run().each(function(result){
    			   // .run().each has a limit of 4,000 results
    				var itemDesignRec = record.create({ type : DESIGN_ORDER_ITEM_DESIGN_REC_TYPE_ID, isDynamic : true});
	    			itemDesignRec.setValue('custrecord_sta_tx_itemdesign_parentlink', result.id);
	    			itemDesignRec.setValue('custrecord_sta_tx_itemdesign_appr', 1);
	    			itemDesignRec.setValue('custrecord_sta_tx_itemdesign_do', designOrderId);
	    			itemDesignRec.save();
    			   return true;
    			});
    	}
    	return designOrderId;
    }
    
    //4 - set design order item teamshop
    function onSetItemTeamshop(designOrderId, teamshopId){
    	if(designOrderId){
    		var customrecord_sta_tx_dodetSearchObj = search.create({ type: "customrecord_sta_tx_dodet", filters:[["custrecord_sta_tx_dodet_parentlink","anyof",designOrderId]], columns:[] });
			customrecord_sta_tx_dodetSearchObj.run().each(function(result){
				record.submitFields({ type : DESIGN_ORDER_ITEM_REC_TYPE_ID, id : result.id, values : {custrecord_sta_tx_dodet_teamshop : teamshopId}});
			   return true;
			});
    	}
    	return designOrderId;
    }
    
    //5 - set design order item desing teamshop
    function onSetDesignTeamshop(designOrderId, teamshopId){
    	if(designOrderId){
    		var customrecord_sta_tx_itemdesignSearchObj = search.create({ type: "customrecord_sta_tx_itemdesign", filters:[["custrecord_sta_tx_itemdesign_do","anyof",designOrderId]],columns:[]});
    		customrecord_sta_tx_itemdesignSearchObj.run().each(function(result){
				record.submitFields({ type : DESIGN_ORDER_ITEM_DESIGN_REC_TYPE_ID, id : result.id, values : { custrecord_sta_tx_itemdesign_teamshop : teamshopId}});
			   return true;
			});
    	}
    	return designOrderId;
    }
    
    //6 - reset all item designs status
    function onUpdateAllDesigns(id){
    	Ext.Msg.wait('Updating item design status...', 'Update Design Status');
    	var slUrl = resolveURL(DESIGN_ORDER_SUITELET_SCRIPT_ID, 'customdeploy_tx_do_reset_design_status', {designOrderId : id});
    	var createDesignOrderPromise = https.get.promise({ url : slUrl })
    		.then(function(response){
	    		Ext.Msg.hide();
	    		location.reload();
	    	})['catch'](function(reason){
	    		Ext.Msg.hide();
	    		message.create({ title : 'Unable to update item design status.', message : reason, type : message.Type.WARNING }).show({duration: 5000});
	    	});
    }
    
    //7 - reflect item status on design list
    function onUpdateItemStatus(id){
    		Ext.Msg.wait('Updating item status...', 'Update Item Status');
        	var slUrl = resolveURL(DESIGN_ORDER_SUITELET_SCRIPT_ID, 'customdeploy_tx_do_reflect_item_status', {designOrderId : id});
        	var createDesignOrderPromise = https.get.promise({ url : slUrl })
	        	.then(function(response){ location.reload(); })
	        	['catch'](function(reason){
	        		Ext.Msg.hide();
	        		message.create({ title : 'Unable to update item status.', message : reason, type : message.Type.WARNING }).show({duration: 5000});
	        	});
    		
    }
    
    //8 - export catalog from teamshop
    function onExportTeamshop(id, recordType, contractId){
    	if(!id && !contractId)return false;
    	
    	var createDesignOrderPromise = https.post.promise({ url : IO_EXPORT_TEAMSHOP_WEBHOOK_URL, body : {"id" : id, "contract-id" : contractId, "recordType" : recordType} })
        	.then(function(response){ location.reload(); })
        	['catch'](function(reason){
        		Ext.Msg.hide();
        		message.create({ title : 'Unable to export teamshop.', message : reason, type : message.Type.WARNING }).show({duration: 5000});
        	});
    }
    
    //9 - export catalog from design order
    function onExportDesignOrder(id, recordType){
    	if(!id && !recordType)return false;
    	
    	var createDesignOrderPromise = https.post.promise({ url : IO_EXPORT_DESIGN_ORDER_WEBHOOK_URL, body : {"id" : id, "recordType" : recordType} })
        	.then(function(response){ location.reload(); })
        	['catch'](function(reason){
        		Ext.Msg.hide();
        		message.create({ title : 'Unable to export design order.', message : reason, type : message.Type.WARNING }).show({duration: 5000});
        	});
    }
    
    //10 - update design order items
    function onUpdateDesignOrderItems(offerId){
    	if(offerId){
    		var slUrl = resolveURL(DESIGN_ORDER_SUITELET_SCRIPT_ID, 'customdeploy_tx_do_update_item', {offerId : offerId});
        	//console.log(slUrl);
        	var updateDesignOrderPromise = https.get.promise({ url : slUrl })
        		.then(function(response){
        			location.reload();
    	    	})['catch'](function(reason){
    	    		//Ext.Msg.hide();
    	    		message.create({ title : 'Unable to update design order', message : reason, type : message.Type.WARNING }).show({duration: 5000});
    	    	});	
    	}
    }
    
    //Extra - UE utility function
    function onSetCustomDesingId(designOrderId){
    	if(designOrderId){
    		var customrecord_sta_tx_itemdesignSearchObj = search.create({
 			   type: "customrecord_sta_tx_itemdesign",
 			   filters:[["custrecord_sta_tx_itemdesign_do","anyof",designOrderId]],
 			   columns:[{name : 'name'}, {name : 'custrecord_sta_tx_itemdesign_id'}]
 			});
    		customrecord_sta_tx_itemdesignSearchObj.run().each(function(result){
    			if(result.getValue('name') != result.getValue('custrecord_sta_tx_itemdesign_id')){
    				record.submitFields({
     		    		type : DESIGN_ORDER_ITEM_DESIGN_REC_TYPE_ID,
     		    		id : result.id,
     		    		values : {
     		    			custrecord_sta_tx_itemdesign_id : result.getValue('name')
     		    		}
     		    	});
    			}
 			   return true;
 			});
    	}
    }
    //END - User Event button/client functions
    
    //STAT - NetSuite Standard Client Functions
    /**
     * Function to be executed when field is changed.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     * @param {string} scriptContext.fieldId - Field name
     * @param {number} scriptContext.lineNum - Line number. Will be undefined if not a sublist or matrix field
     * @param {number} scriptContext.columnNum - Line number. Will be undefined if not a matrix field
     *
     * @since 2015.2
     */
    function fieldChanged(scriptContext) {
    	if(scriptContext.fieldId == 'custrecord_sta_tx_itemdesign_updated' && scriptContext.currentRecord.getValue('custrecord_sta_tx_itemdesign_updated')){
    		scriptContext.currentRecord.setValue('custrecord_sta_tx_itemdesign_appr', 1);
    	}
    }
    
    /**
     * Function to be executed after page is initialized.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.mode - The mode in which the record is being accessed (create, copy, or edit)
     *
     * @since 2015.2
     */
    function pageInit(scriptContext){
    	if(scriptContext.currentRecord.type == DESIGN_ORDER_REC_TYPE_ID){
    		//Need to fix permission ID - unable to remove Add button on item list
    		if([runtime.Permission.NONE, runtime.Permission.VIEW].indexOf(runtime.getCurrentUser().getPermission(DESIGN_ORDER_ITEM_PERMISSION_ID)) == -1 || runtime.getCurrentUser().role == 3){
				itemButtons = document.getElementById('recmachcustrecord_sta_tx_dodet_parentlink_buttons');
				if(itemButtons){
					itemButtons.getElementsByClassName('uir-addedit bntBg')[0].remove()
		    		itemButtons.getElementsByClassName('uir-machinebutton-separator')[0].remove()
		    		itemButtons.getElementsByClassName('uir-insert bntBg')[0].remove()
				}
			}
    		
    		//Removes "do you want to leave this page" message from browser
    		window.onbeforeunload = null;
    		
    		//Removes unnecessary buttons from sublists
    		Machine.prototype.copyline = function Machine_copyline() {
    			if (!NS.form.isInited()) {
    				return false;
    			}
    			var that = this;
    			var linenum = parseInt(this.getMachineIndex());
    			if (linenum == this.getNextIndex()) {
    				linenum -= 1;
    				loadNewLine();
    			} else {
    				if (this.ischanged) {
    					this.addline();
    				}
    				if (NS.form.isInited()) {
    					createAndLoadNewLine();
    				} else {
    					NS.event.once(NS.event.type.FORM_INITED, function () {
    						createAndLoadNewLine();
    					});
    				}
    			}
    			return true;

    			function createAndLoadNewLine() {
    				that.clearline(false, that.lastStartSegmentIndex);
    				loadNewLine();
    			}

    			function loadNewLine() {
    				that.loadline(linenum, true);
    				for (var i = 0; i < that.countFormElements(); i++) {
    					if (that.getFormElementType(i) == "slaveselect") {
    						
    						eval(getSyncFunctionName(that.getFormElementName(i), that.name) + "(true, " + (linenum - 1).toString() + ", true);");
    					}
    				}
    				that.synclinefields(linenum);
    				/*if (that.isRecordManagerUsed()) {
    					that.dataManager.setInitialFieldValues(that.getLineData(), that.dataManager.activeRowIdx);
    				}*/
    				that.buildtable();
    				that.ischanged = true;
    				that.isinserting = false;
    				nlapiSetCurrentLineItemValue('recmachcustrecord_sta_tx_itemdesign_do', 'id', "");
    				nlapiSetCurrentLineItemValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord_sta_tx_itemdesign_matrix', 'F');
    			}
    		};
        	
    		//Removes insert button from design list
        	btnTable = document.getElementById('recmachcustrecord_sta_tx_itemdesign_do_buttons');
        	if(btnTable){
        		btnTable.getElementsByClassName('uir-insert bntBg')[0].remove();
        		mainBtnTable = btnTable.getElementsByTagName('table')[0];
        		copyButtonCell = mainBtnTable.getElementsByTagName('tr')[0].insertCell(4);
        		copyButtonCell.innerHTML = '<td> <table class="machBnt" id="tbl_recmachcustrecord_sta_tx_itemdesign_do_copy" cellpadding="0" cellspacing="0" border="0" style="cursor:hand;" role="presentation"> <tbody><tr><td class="bntTop"><img src="/images/nav/ns_x.gif" width="1" height="3" alt=""></td></tr> <tr class=""><td class="uir-copy bntBg" valign="top"> <input type="button" style="" class="nlinlineeditbutton bntBgT" value="Make Copy" id="recmachcustrecord_sta_tx_itemdesign_do_copy" name="recmachcustrecord_sta_tx_itemdesign_do_copy" onclick="recmachcustrecord_sta_tx_itemdesign_do_machine.copyline();setWindowChanged(window, true); return false;" onmousedown="this.setAttribute("_mousedown","T"); setButtonDown(true, true, this);" onmouseup="this.setAttribute("_mousedown","F"); setButtonDown(false, true, this);" onmouseout="if(this.getAttribute("_mousedown")=="T") setButtonDown(false, true, this);" onmouseover="if(this.getAttribute("_mousedown")=="T") setButtonDown(true, true, this);" _mousedown="F"></td> </tr> <tr><td class="bntBot"><img src="/images/nav/ns_x.gif" width="1" height="3" alt=""></td></tr> </tbody></table> </td>';
        	}
    	}
    }
    
    /**
     * Validation function to be executed when record is saved.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @returns {boolean} Return true if record is valid
     *
     * @since 2015.2
     */
    function saveRecord(scriptContext){
		//Ext.Msg.wait('Submitting design order...', 'Design Order');
		isTrue = false;
    	setItemTeamshopRequest = true;
    	
    	//if(!scriptContext.currentRecord.getValue('custrecord_sta_tx_do_teamshop')){
        	
    		var slUrl = resolveURL(DESIGN_ORDER_SUITELET_SCRIPT_ID, 'customdeploy_tx_do_set_item_teamshop', {designOrderId : scriptContext.currentRecord.id, teamshopId : scriptContext.currentRecord.getValue('custrecord_sta_tx_do_teamshop')});
        	setItemTeamshopRequest = https.get.promise({ url : slUrl })
        	.then(function(response){        		
        		return true;
        	})['catch'](function(reason){
        		//Ext.Msg.hide();
        		message.create({ title : 'Unable to apply teamshop to item design.', message : reason, type : message.Type.WARNING }).show({duration: 5000});
        		return false;
        	})['finally'](function(){ return true; });
    	//}
    	
    	return setItemTeamshopRequest;
    }
    
    function lineInit(scriptContext){
    	//if(scriptContext.sublistId == 'recmachcustrecord_sta_tx_dodet_parentlink'){
    		/*console.log(scriptContext)
    		itemListHTML = document.getElementsByClassName('uir-machine-row uir-machine-row-odd listtextnonedit');
    		if(!itemListHTML && itemListHTML.length > 0) itemListHTML[0].remove();*/
    		
    		//itemListHTML = document.getElementsByClassName('uir-machine-row uir-machine-row-odd listtextnonedit uir-machine-row-focused');
    		//if(!itemListHTML && itemListHTML.length > 0) itemListHTML[0].remove();
    	//}
    	//Removes new item and design buttons 
		//tbl_newrecrecmachcustrecord_sta_tx_dodet_parentlink
		//tbl_newrecrecmachcustrecord_sta_tx_itemdesign_do
		console.log('test');
    	if(document.getElementById('tbl_newrecrecmachcustrecord_sta_tx_dodet_parentlink'))
			document.getElementById('tbl_newrecrecmachcustrecord_sta_tx_dodet_parentlink').remove();
		if(document.getElementById('tbl_newrecrecmachcustrecord_sta_tx_itemdesign_do'))
			document.getElementById('tbl_newrecrecmachcustrecord_sta_tx_itemdesign_do').remove();
    }
  //END - NetSuite Standard Client Functions
    
    return {
    	//NS Client Functions
    	pageInit : pageInit,
    	lineInit : lineInit,
    	fieldChanged : fieldChanged,
    	saveRecord : saveRecord,
    	
    	//Reference Functions
        createDesignOrder : onCreateDesignOrder,
        createMatrixItem : onCreateMatrixItem,
        createDesignNumber : createDesignNumber,
        createItemDesign : onCreateDefaultItemDesign,
        setItemTeamshop : onSetItemTeamshop,
        setDesignTeamshop : onSetDesignTeamshop,
        updateAllDesigns : onUpdateAllDesigns,
        updateItemStatus : onUpdateItemStatus,
        setDesignId : onSetCustomDesingId,
        getDesignNumbers : onGetDesignNumbers,
        exportTeamshop : onExportTeamshop,
        exportDesignOrder : onExportDesignOrder,
        updateDesignOrderItems : onUpdateDesignOrderItems
    };
    
});
