/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/record'],

function(record) {
   
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {

    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {
    	var ctx = scriptContext;
    	if(['create', 'edit'].indexOf(ctx.type) != -1){
    		var discountList = [];
    		for(var i = 0; i < ctx.newRecord.getLineCount('item'); i++){
    			var discountObj = {};
    			var discountAmt = ctx.newRecord.getSublistValue('item', 'custcol_sta_tx_dw_discount', i);
    			if(discountAmt && discountAmt != 0){
    				discountObj.grossAmout = discountAmt;
    				discountObj.description = ctx.newRecord.getSublistValue('item', 'custcol_sta_tx_dw_discount_desc', i);
    				discountObj.appliedToLine = ctx.newRecord.getSublistValue('item', 'line', i);
    				discountList.push(discountObj);
    			}
    		}
    		log.debug('discountList', discountList);
    		
    		discountList.forEach(function(discountObj){
    			var i = ctx.newRecord.findSublistLineWithValue({sublistId: 'item', fieldId:'line', value: discountObj.appliedToLine});
    			i++;
    			
    			ctx.newRecord.insertLine('item', i, true);
    			ctx.newRecord.setSublistValue('item', 'item', i, 1391);
    			ctx.newRecord.setSublistValue('item', 'price', i, -1);
    			ctx.newRecord.setSublistValue('item', 'grossamt', i, discountObj.grossAmout);
    			ctx.newRecord.setSublistValue('item', 'description', i, discountObj.description);
    			ctx.newRecord.setSublistValue('item', 'custcol_so_rowposition', i, '');
    			//ctx.newRecord.commitLine('item');
    		});
    	}
    }
    
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	var ctx = scriptContext;
    	if(['create', 'edit'].indexOf(ctx.type) != -1){
    		var salesOrderRec = record.load({type: ctx.newRecord.type, id: ctx.newRecord.id})
    		var discountList = [];
    		for(var i = 0; i < salesOrderRec.getLineCount('item'); i++){
    			var discountObj = {};
    			var discountAmt = salesOrderRec.getSublistValue('item', 'custcol_sta_tx_dw_discount', i);
    			if(discountAmt && discountAmt != 0){
    				discountObj.grossAmout = discountAmt;
    				discountObj.description = salesOrderRec.getSublistValue('item', 'custcol_sta_tx_dw_discount_desc', i);
    				discountObj.appliedToLine = salesOrderRec.getSublistValue('item', 'line', i);
    				discountList.push(discountObj);
    			}
    		}
    		log.debug('discountList', discountList);
    		
    		//var salesOrderRec = record.load({type: ctx.newRecord.type, id: ctx.newRecord.id})
    		
    		discountList.forEach(function(discountObj){
    			var i = salesOrderRec.findSublistLineWithValue({sublistId: 'item', fieldId:'line', value: discountObj.appliedToLine});
    			
    			salesOrderRec.setSublistValue('item', 'custcol_sta_tx_dw_discount', i, '');
    			salesOrderRec.setSublistValue('item', 'custcol_sta_tx_dw_discount_desc', i, '');
    			
    			i++;
    			
    			salesOrderRec.insertLine('item', i, true);
    			salesOrderRec.setSublistValue('item', 'item', i, 1391);
    			salesOrderRec.setSublistValue('item', 'price', i, -1);
    			salesOrderRec.setSublistValue('item', 'grossamt', i, discountObj.grossAmout);
    			salesOrderRec.setSublistValue('item', 'description', i, discountObj.description);
    			salesOrderRec.setSublistValue('item', 'custcol_so_rowposition', i, '');
    			//ctx.newRecord.commitLine('item');
    		});
    		
    		salesOrderRec.save();
    	}
    }

    return {
        //beforeLoad: beforeLoad,
        //beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
