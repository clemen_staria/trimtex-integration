/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope SameAccount
 */
define(['N/ui/serverWidget', 'N/record', 'N/search', 'N/error', 'N/runtime', 'N/redirect','./trimtex_design_order_cs_v2.js'],

function(ui, record, search, error, runtime, redirect, designorder) {
	var ESTIMATE_REC_TYPE_ID = 'estimate';
	var TEAMSHOP_REC_TYPE_ID = 'customrecord_sta_teamshop';
	var DESIGN_ORDER_REC_TYPE_ID = 'customrecord_sta_tx_do';
	var DESIGN_ORDER_ITEM_REC_TYPE_ID = 'customrecord_sta_tx_dodet';
	var DESIGN_ORDER_ITEM_DESIGN_REC_TYPE_ID = 'customrecord_sta_tx_itemdesign';
	var DESIGN_ORDER_SUITELET_SCRIPT_ID = 'customscript_tx_design_order_sl';
	var OFFER_ENTITY_STATUS_DEFAULT = 10;// Design Order
	
	//Design order status
	var DESIGN_ORDER_STATUS_STARTED = 1;
	var DESIGN_ORDER_STATUS_DRAFT_IN_PROGRESS = 2;
	var DESIGN_ORDER_STATUS_DRAFT_PENDING_APPROVAL = 3;
	var DESIGN_ORDER_STATUS_DRAFT_APPROVED = 4;
	var DESIGN_ORDER_STATUS_IN_PROGRESS = 5;
	var DESIGN_ORDER_STATUS_READY = 6;
	var DESIGN_ORDER_STATUS_PENDING_CUSTOMER_APPROVAL = 7;
	var DESIGN_ORDER_STATUS_CUSTOMER_APPROVED = 8;
	var DESIGN_ORDER_STATUS_CLOSED = 9;
	
	
    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {string} scriptContext.type - Trigger type
     * @param {Form} scriptContext.form - Current form
     * @Since 2015.2
     */
    function beforeLoad(scriptContext) {
    	//Load client script library
    	scriptContext.form.clientScriptModulePath = './trimtex_design_order_cs_v2.js';
    	
    	switch (scriptContext.newRecord.type){
    	case ESTIMATE_REC_TYPE_ID:
    		beforeLoad_Estimate(scriptContext);
    		break;
    	case DESIGN_ORDER_REC_TYPE_ID:
    		beforeLoad_DesignOrder(scriptContext);
    		break;
    	case DESIGN_ORDER_ITEM_REC_TYPE_ID:
    		beforeLoad_DesignOrderItem(scriptContext);
    		break;
    	case DESIGN_ORDER_ITEM_DESIGN_REC_TYPE_ID:
    		beforeLoad_DesignOrderItemDesign(scriptContext);
    		break;
    	case TEAMSHOP_REC_TYPE_ID:
    		beforeLoad_Teamshop(scriptContext);
    		break;
    	}
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function beforeSubmit(scriptContext) {
    	switch (scriptContext.newRecord.type){
    	case ESTIMATE_REC_TYPE_ID:
    		beforeSubmit_Estimate(scriptContext);
    		break;
    	case DESIGN_ORDER_REC_TYPE_ID:
    		beforeSubmit_DesignOrder(scriptContext);
    		break;
    	}
    }

    /**
     * Function definition to be triggered before record is loaded.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.newRecord - New record
     * @param {Record} scriptContext.oldRecord - Old record
     * @param {string} scriptContext.type - Trigger type
     * @Since 2015.2
     */
    function afterSubmit(scriptContext) {
    	switch (scriptContext.newRecord.type){
    	case ESTIMATE_REC_TYPE_ID:
    		afterSubmit_Estimate(scriptContext);
    		break;
    	case DESIGN_ORDER_REC_TYPE_ID:
    		afterSubmit_DesignOrder(scriptContext);
    		break;
    	case DESIGN_ORDER_ITEM_REC_TYPE_ID:
    		afterSubmit_DesignOrderItem(scriptContext);
    		break;
    	case DESIGN_ORDER_ITEM_DESIGN_REC_TYPE_ID:
    		afterSubmit_DesignOrderItemDesign(scriptContext);
    		break;
    	}
    }
    
    //1 - design order beforeLoad
    var beforeLoad_DesignOrder = function(scriptContext){
    	switch (scriptContext.type){
    	case 'view':
    		//If approved, show Create Matrix Item button
    		if(scriptContext.newRecord.getValue('custrecord_sta_tx_do_status') == 8){
    			if(['customrole_sta_tx_designer','customrole_sta_tx_designer_new'].indexOf(runtime.getCurrentUser().roleId) == -1){
					var customrecord_sta_tx_dolinkSearchObj_2 = search.create({ type: "customrecord_sta_tx_itemdesign",
	    				   filters:[["custrecord_sta_tx_itemdesign_do","anyof",scriptContext.newRecord.id], "AND", ["custrecord_sta_tx_itemdesign_appr", "anyof", 2], "AND", ["custrecord_sta_tx_itemdesign_matrix", "is", false]],
	    				   columns:[]
	    				});
					
					if(customrecord_sta_tx_dolinkSearchObj_2.runPaged().count > 0)
						scriptContext.form.addButton({ id : 'custpage_create_matrix_item', label : 'Create Matrix Item', functionName : 'createMatrixItem('+ scriptContext.newRecord.id +','+ scriptContext.newRecord.getValue('custrecord_sta_tx_do_teamshop') +')'});
				}
    		}
    		
    		//Show Update All Designs button
			var customrecord_sta_tx_dolinkSearchObj_1 = search.create({ type: "customrecord_sta_tx_itemdesign",
				   filters:[["custrecord_sta_tx_itemdesign_do","anyof",scriptContext.newRecord.id], "AND", ["custrecord_sta_tx_itemdesign_appr", "noneof", [1, 3]]],
				   columns:[]
				});
			//log.debug('customrecord_sta_tx_dolinkSearchObj_1', customrecord_sta_tx_dolinkSearchObj_1.runPaged().count);
			//log.debug(scriptContext.newRecord.getValue('custrecord_sta_tx_do_status'));
			if(customrecord_sta_tx_dolinkSearchObj_1.runPaged().count > 0 && ['8'].indexOf(scriptContext.newRecord.getValue('custrecord_sta_tx_do_status')) > -1){
				scriptContext.form.addButton({ id : 'custpage_update_all_design', label : 'Update All Designs', functionName : 'updateAllDesigns('+ scriptContext.newRecord.id +')'});
			}
			
    		if(scriptContext.newRecord.getValue('custrecord_sta_dx_do_matrix_status') == 'COMPLETE'){
				scriptContext.form.addButton({ id : 'custpage_export_to_dw', label : 'Export to DW', functionName : 'exportDesignOrder('+ scriptContext.newRecord.id +',"DO")'});
    		}
    		//Always show Refresh Item Status button
    		/*if(['customrole_sta_tx_designer', 'customrole1043'].indexOf(runtime.getCurrentUser().roleId) != -1){
    			scriptContext.form.addButton({id : 'custpage_refreshitemstatus', label : 'Refresh Item Status', functionName : 'updateItemStatus('+ scriptContext.newRecord.id +')'});
    		}*/
    		
    		//scriptContext.form.getSublist('recmachcustrecord_sta_tx_dodet_parentlink').removeButton('newrecrecmachcustrecord_sta_tx_dodet_parentlink');
    		removeSublistNewButtons(scriptContext);
    		break;
    	case 'create':
    		if(scriptContext.request){
				if(scriptContext.request.parameters.entityId) scriptContext.newRecord.setValue('custrecord_sta_tx_do_custname', scriptContext.request.parameters.entityId);
    			if(scriptContext.request.parameters.entityId)scriptContext.newRecord.setValue('custrecord_sta_tx_do_status', scriptContext.request.parameters.statusId);
    			if(scriptContext.request.parameters.entityId){
    				scriptContext.newRecord.setValue('custrecord_sta_tx_do_offer', scriptContext.request.parameters.offerId);

    				var designOrderSourceRec = record.load({ type : DESIGN_ORDER_REC_TYPE_ID, id : scriptContext.request.parameters.offerId });
        			
        			scriptContext.newRecord.setValue('custrecord_sta_tx_do_status', 1);
        			
        			
        			for(var i = 0; i < designOrderSourceRec.getLineCount('recmachcustrecord_sta_tx_dodet_parentlink'); i++){
        				var itemId = designOrderSourceRec.getSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_parentitem', i);
        				var modelId = designOrderSourceRec.getSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_modelid', i);            				
        				scriptContext.newRecord.setSublistValue({sublistId : 'recmachcustrecord_sta_tx_dodet_parentlink', 
        					fieldId : 'custrecord_sta_tx_dodet_parentitem', line : i, value : itemId});
        			}
    			}
			}
    		removeSublistNewButtons(scriptContext);
    		break;
    	case 'copy':
    		var designOrderSourceRec = record.load({ type : DESIGN_ORDER_REC_TYPE_ID, id : scriptContext.request.parameters.id });
			
			scriptContext.newRecord.setValue('custrecord_sta_tx_do_status', 1);
			
			for(var i = 0; i < designOrderSourceRec.getLineCount('recmachcustrecord_sta_tx_dodet_parentlink'); i++){
				var itemId = designOrderSourceRec.getSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_parentitem', i);
				var modelId = designOrderSourceRec.getSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_modelid', i);				
				scriptContext.newRecord.setSublistValue({sublistId : 'recmachcustrecord_sta_tx_dodet_parentlink', 
					fieldId : 'custrecord_sta_tx_dodet_parentitem', line : i, value : itemId});
			}
			removeSublistNewButtons(scriptContext);
			break;
    	case 'edit':
    		var designOrderId = scriptContext.newRecord.id
    		var desingOrderFields = search.lookupFields({ type: DESIGN_ORDER_REC_TYPE_ID, id: designOrderId,
    			columns: ['custrecord_sta_tx_do_status', 'custrecord_sta_tx_do_designer', 'custrecord_sta_tx_do_salesrep']
			});
    		
    		switch(desingOrderFields['custrecord_sta_tx_do_status'][0].value){
    		case '2' :
    			if(['customrole_sta_tx_designer', 'customrole_sta_tx_designer_new'].indexOf(runtime.getCurrentUser().roleId) != -1){
    				if(desingOrderFields['custrecord_sta_tx_do_designer'].length == 0){
    					redirectToLockMessage(designOrderId);
    				}else if(desingOrderFields['custrecord_sta_tx_do_designer'].length > 0 && runtime.getCurrentUser().id != desingOrderFields['custrecord_sta_tx_do_designer'][0].value){
    					redirectToLockMessage(designOrderId);
					}
    			}    			
    			if(['customrolecustomrole_sta_tr_salesperson', 'customrole_sta_trimtex_salesmgr_restrict', 'customrole_sta_tr_salesmanager'].indexOf(runtime.getCurrentUser().roleId) != -1){
    				if(runtime.getCurrentUser().id != desingOrderFields['custrecord_sta_tx_do_salesrep'][0].value && desingOrderFields['custrecord_sta_tx_do_designer'].length > 0)
    					redirectToLockMessage(designOrderId);
    			}
    			if(['customrolecustomrole_sta_tr_salesperson'].indexOf(runtime.getCurrentUser().roleId) != -1 && desingOrderFields['custrecord_sta_tx_do_designer'].length > 0){
    					redirectToLockMessage(designOrderId);
    			}
    			
    			break;
    		case '5' :
    			if(['customrolecustomrole_sta_tr_salesperson', 'customrole_sta_trimtex_salesmgr_restrict', 'customrole_sta_tr_salesmanager'].indexOf(runtime.getCurrentUser().roleId) != -1){
    				if(runtime.getCurrentUser().id != desingOrderFields['custrecord_sta_tx_do_salesrep'][0].value && desingOrderFields['custrecord_sta_tx_do_designer'].length > 0)
    					redirectToLockMessage(designOrderId); 
    			}
    			
    			if(['customrolecustomrole_sta_tr_salesperson'].indexOf(runtime.getCurrentUser().roleId) != -1 && desingOrderFields['custrecord_sta_tx_do_designer'].length > 0){
    				redirectToLockMessage(designOrderId); 
    			}
    			
    			if(['customrole_sta_tx_designer', 'customrole_sta_tx_designer_new'].indexOf(runtime.getCurrentUser().roleId) != -1){
    				if(desingOrderFields['custrecord_sta_tx_do_designer'].length == 0){
    					redirectToLockMessage(designOrderId);
    				}else if(desingOrderFields['custrecord_sta_tx_do_designer'].length > 0 && runtime.getCurrentUser().id != desingOrderFields['custrecord_sta_tx_do_designer'][0].value){
    					redirectToLockMessage(designOrderId);
					}
    			}
    			break;
    		case '9' :
    			if(['administrator'].indexOf(runtime.getCurrentUser().roleId) == -1){
    				redirectToLockMessage(designOrderId);
    			}
    			break;
    		default :
    			if(['customrole_sta_tx_designer', 'customrole_sta_tx_designer_new'].indexOf(runtime.getCurrentUser().roleId) != -1){
    				redirectToLockMessage(designOrderId);
    			}
    		
	    		if(['customrolecustomrole_sta_tr_salesperson', 'customrole_sta_trimtex_salesmgr_restrict', 'customrole_sta_tr_salesmanager'].indexOf(runtime.getCurrentUser().roleId) != -1){
    				if(runtime.getCurrentUser().id != desingOrderFields['custrecord_sta_tx_do_salesrep'][0].value && desingOrderFields['custrecord_sta_tx_do_designer'].length > 0)
    					redirectToLockMessage(designOrderId);
    			}
    			break;
    		}
    		removeSublistNewButtons(scriptContext);
    		break;
    	}
    };
    
    //2 - design order item beforeLoad
    var beforeLoad_DesignOrderItem = function(scriptContext){
    	switch (scriptContext.type){
    	case 'view':
    		break;
    	case 'create':
    		break;
    	case 'copy':
    		break;
    	case 'edit':
    		//Disable parent item field
        	scriptContext.form.getField({id : 'custrecord_sta_tx_dodet_parentitem'}).updateDisplayType({displayType : ui.FieldDisplayType.DISABLED});
    		break;
    	}
    }
    
    //3 - design order item design beforeLoad
    var beforeLoad_DesignOrderItemDesign = function(scriptContext){
    	
    };
    
    //4 - estimate beforeLoad
    var beforeLoad_Estimate = function(scriptContext){
    	switch (scriptContext.type){
    	case 'view':
    		if(!scriptContext.newRecord.getValue('custbody_sta_tx_do') && scriptContext.newRecord.getValue('entitystatus') !== '14' &&
    				scriptContext.newRecord.getValue('salesrep') == runtime.getCurrentUser().id && ['PENDING'].indexOf(scriptContext.newRecord.getValue('custbody_sta_tx_do_create_status')) < 0)
	    		scriptContext.form.addButton({
	    			id : 'custpage_create_design_order',
	    			label : 'Create Design Order',
	    			functionName : 'createDesignOrder(' + scriptContext.newRecord.id + ',' + scriptContext.newRecord.getValue('entity') +')'
	    		});
    		
    		if(scriptContext.newRecord.getValue('custbody_sta_tx_do'))
    			scriptContext.form.addButton({
	    			id : 'custpage_update_design_order',
	    			label : 'Update Design Order Items',
	    			functionName : 'updateDesignOrderItems(' + scriptContext.newRecord.id + ')'
	    		});
    		
    		break;
    	}
    };
    
    var beforeLoad_Teamshop = function(scriptContext){
    	switch (scriptContext.type){
    	case 'view':
    		scriptContext.form.addButton({
    			id : 'custpage_export_to_demandware',
    			label : 'Export to DW',
    			functionName : "exportTeamshop('" + scriptContext.newRecord.id + "','TEAMSHOP','" + scriptContext.newRecord.getValue('custrecord_sta_teamshop_contractid') +"')"
    		});
    		break;
    	}
    };
    
    //5 - design order after submit
    var afterSubmit_DesignOrder = function(scriptContext){
    	switch (scriptContext.type){
    	case 'view':
    		break;
    	case 'create':
    		//Set DO reference on Offer
    		record.submitFields({ type : record.Type.ESTIMATE, id : scriptContext.newRecord.getValue({fieldId : 'custrecord_sta_tx_do_offer'}),
    			values : { custbody_sta_tx_do : scriptContext.newRecord.id, custbody_sta_tx_do_create_status : 'COMPLETE', entitystatus : OFFER_ENTITY_STATUS_DEFAULT}
    		});
    		
			//Set design order item design id (custom field) - utility
    		designorder.setDesignId(scriptContext.newRecord.id);
    		break;
    	case 'copy':
    		break;
    	case 'edit':
    		if(scriptContext.newRecord.getValue('custrecord_sta_tx_do_status') == 8){
				for(var i = 0; i < scriptContext.newRecord.getLineCount('recmachcustrecord_sta_tx_dodet_parentlink'); i++){
					if(scriptContext.newRecord.getSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_itemstatus', i) == 2){
						var designRec = record.load({type: DESIGN_ORDER_ITEM_REC_TYPE_ID, id: scriptContext.newRecord.getSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'id', i)});
						for(var j = 0; j < designRec.getLineCount('recmachcustrecord_sta_tx_itemdesign_parentlink'); j++){
							if(designRec.getSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_appr', j) == 4){
								designRec.setSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_appr', j, 2);
    							designRec.setSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_updated', j, false);
    							designRec.setSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_do', j, scriptContext.newRecord.id);
							}
						}
						designRec.save();
					}
				}
				
				for(var j = 0; j < scriptContext.newRecord.getLineCount('recmachcustrecord_sta_tx_itemdesign_do'); j++){
					if(scriptContext.newRecord.getSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord_sta_tx_itemdesign_updated', j)){
						
						if(scriptContext.newRecord.getSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord_sta_tx_itemdesign_appr', j) != 3)
							record.submitFields({ type : DESIGN_ORDER_ITEM_DESIGN_REC_TYPE_ID,
		            			id : scriptContext.newRecord.getSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'id', j),
		            			values : { custrecord_sta_tx_itemdesign_appr : 1 }
		            		});
						
						record.submitFields({ type : DESIGN_ORDER_ITEM_REC_TYPE_ID,
	            			id : scriptContext.newRecord.getSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord_sta_tx_itemdesign_parentlink', j),
	            			values : { custrecord_sta_tx_dodet_itemstatus : 1 }
	            		});
						
						/*record.submitFields({ type : DESIGN_ORDER_REC_TYPE_ID, id : scriptContext.newRecord.id,
	            			values : { custrecord_sta_tx_do_status : 7 }
	            		});*/
					}
				}
    		}
    		
    		//Set design order item design id (custom field) - utility
    		designorder.setDesignId(scriptContext.newRecord.id);
    		break;
    	}
    };
    
    //6 - design order item after submit
    var afterSubmit_DesignOrderItem = function(scriptContext){
    	switch (scriptContext.type){
    	case 'view':
    		break;
    	case 'create':
    		//Create default item design
    		log.debug(runtime.executionContext);
    		if(runtime.executionContext != runtime.ContextType.USER_INTERFACE){
    			designorder.createItemDesign(scriptContext.newRecord.getValue('custrecord_sta_tx_dodet_parentlink'));
    		}
    		break;
    	case 'copy':
    		break;
    	case 'edit':
    		break;
    	}
    };
    
    //7 - design order item design after submit
    var afterSubmit_DesignOrderItemDesign = function(scriptContext){
    	
    };
    
    //8 - estimate after submit
    var afterSubmit_Estimate = function(scriptContext){
    	
    };
    
  //9 - estimate before submit
    var beforeSubmit_Estimate = function(scriptContext){
    	if(scriptContext.type == 'edit'){
    		if(!scriptContext.newRecord.getValue('custbody_sta_tx_do') && scriptContext.oldRecord.getValue('custbody_sta_tx_do') && scriptContext.newRecord.getValue('entitystatus') == OFFER_ENTITY_STATUS_DEFAULT){
        		scriptContext.newRecord.setValue('entitystatus', 9);//8 offer in SB, 9 offer in Prod
        		scriptContext.newRecord.setValue('custbody_sta_tx_do_create_status', '');
        	}
    	}
    };
    
    var beforeSubmit_DesignOrder = function(scriptContext){
    	switch (scriptContext.type){
    	case 'edit':
    		
    		for (var i = 0; i < scriptContext.newRecord.getLineCount('recmachcustrecord_sta_tx_itemdesign_do'); i++){
    			//scriptContext.newRecord.selectLine('recmachcustrecord_sta_tx_itemdesign_do', i);
    			var itemIndex = scriptContext.newRecord.findSublistLineWithValue('recmachcustrecord_sta_tx_dodet_parentlink', 'id', 
    					scriptContext.newRecord.getSublistValue('recmachcustrecord_sta_tx_itemdesign_do','custrecord_sta_tx_itemdesign_parentlink', i))
    			
        		if(itemIndex > -1){
        			scriptContext.newRecord.setSublistValue('recmachcustrecord_sta_tx_itemdesign_do', 'custrecord1400', i,
        					scriptContext.newRecord.getSublistValue('recmachcustrecord_sta_tx_dodet_parentlink', 'custrecord_sta_tx_dodet_itemstatus', itemIndex));
        			//scriptContext.newRecord.commitLine('recmachcustrecord_sta_tx_itemdesign_do');
        		}
    		}
    		
    		break;
    	}
    };
    
    //Extra function to lock design order
    var redirectToLockMessage = function(designOrderId){
    	redirect.toSuitelet({
			scriptId : DESIGN_ORDER_SUITELET_SCRIPT_ID,
    		deploymentId : 'customdeploy_tx_do_lock_design_order',
    		parameters : {
    			designOrderId : designOrderId
    		}
		})
    }
    
    var removeSublistNewButtons = function(scriptContext){
    	var inline = scriptContext.form.addField({
            id : 'custpage_html',
            type : 'inlinehtml',
            label : 'Text'
        });
		inline.defaultValue = '<script>\
			var i,do_until_fn = function(){ console.log("okok");\
    		if(document.getElementById("tbl_newrecrecmachcustrecord_sta_tx_dodet_parentlink")){\
    		document.getElementById("tbl_newrecrecmachcustrecord_sta_tx_dodet_parentlink").remove();}\
			if(document.getElementById("tbl_newrecrecmachcustrecord_sta_tx_itemdesign_do")){\
			document.getElementById("tbl_newrecrecmachcustrecord_sta_tx_itemdesign_do").remove();}\
			if(document.getElementById("tbl_newrec599")){\
			document.getElementById("tbl_newrec599").remove();}\
			if(document.getElementById("tbl_newrec602")){\
			document.getElementById("tbl_newrec602").remove();}\
			clearInterval(i);};\
			i=setInterval(do_until_fn, 1000);</script>';
    }
    //tbl_newrec599//tbl_newrec602
    return {
        beforeLoad: beforeLoad,
        beforeSubmit: beforeSubmit,
        afterSubmit: afterSubmit
    };
    
});
