/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */
define([],

function() {
    
    /**
     * Function to be executed after line is selected.
     *
     * @param {Object} scriptContext
     * @param {Record} scriptContext.currentRecord - Current form record
     * @param {string} scriptContext.sublistId - Sublist name
     *
     * @since 2015.2
     */
    function lineInit(scriptContext) {
    	console.log(scriptContext.currentRecord.getCurrentSublistIndex(scriptContext.sublistId))
    	if(scriptContext.sublistId == 'recmachcustrecord_sta_tx_itemdesign_parentlink'){    		
    		
    		scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_genericitem', scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_parentitem'));
    		scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_parentitem', scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_itemname'), true, true);
    		scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_modelid', scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_modelid'), true, true);
    		scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_do', scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_parentlink'));

    		var stTeamshop = scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_teamshop');
    		if(trim(stTeamshop))
    			scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_teamshop', stTeamshop, false, true);
    	}
    }
    
    function pageInit(scriptContext){
		scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_do', 
				scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_parentlink'));
    	
    	var stTeamshop = scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_teamshop');
		if(trim(stTeamshop))
			scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_teamshop', stTeamshop, false, true);
    }
    
    function fieldChanged(scriptContext){
    	
    	switch(scriptContext.fieldId){
    	case 'custrecord_sta_tx_dodet_parentitem' :
    		//scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_genericitem', scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_parentitem'), true, true);    		
    		scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_genericitem', scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_parentitem'));
    		scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_parentitem', scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_itemname'), true, true);
    		scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_modelid', scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_modelid'), true, true);
    		scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_do', scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_parentlink'));

    		var stTeamshop = scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_teamshop');
    		if(trim(stTeamshop))
    			scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_teamshop', stTeamshop, false, true);
    		break;
    	case 'custrecord_sta_tx_dodet_itemname' :
    		console.log('custrecord_sta_tx_dodet_itemname')
    		scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_parentitem', scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_itemname'), true, true);
    		break;
    	case 'custrecord_sta_tx_dodet_modelid' : 
    		console.log('custrecord_sta_tx_dodet_modelid')
    		scriptContext.currentRecord.setCurrentSublistValue('recmachcustrecord_sta_tx_itemdesign_parentlink', 'custrecord_sta_tx_itemdesign_modelid', scriptContext.currentRecord.getValue('custrecord_sta_tx_dodet_modelid'), true, true);
    		break;
    	}
    }

    return {
    	//pageInit : pageInit,
        lineInit: lineInit,
        fieldChanged : fieldChanged
    };
    
});
